
# A Python API client for Norad
This project provides Python wrappers for a subset of Norad's HTTP API.

# Status

[![build status](https://gitlab.com/norad/python-api-client/badges/master/build.svg)](https://gitlab.com/norad/python-api-client/commits/master)
[![pypi package](https://img.shields.io/pypi/v/norad.svg)](https://pypi.python.org/pypi/norad)
[![pypi downloads](https://img.shields.io/pypi/dm/norad.svg)](https://pypi.python.org/pypi/norad)

# Current Features
- Configure tests for an organization or a machine
- Scan organizations and machines
- Retrieve scan results for a machine
- List all available organizations
- Create and delete organizations
- Create and delete machines

# Setup

## Install
```bash
python setup.py install
```

## Test
```bash
# Edit `tests/config.ini`.
# Then, run tests:
python -m unittest discover tests
```

# Deployment Notes
Tested with Python 2.7.11 and Python 3.5.1 on Ubuntu 16.04.

Python on OS X seems to link against an old OpenSSL. This project should work
with Python 3 and an upgraded OpenSSL from `brew install`.

# Example Usage

```python
from norad import Client

client = Client(token='YOUR_USER_API_TOKEN')
org = client.organization('dwyde-org-default')    
machines = org.list_machines()
print(machines)

# Create machine
machine = org.create_machine(fqdn='norad.cisco.com')

# Create SSH service
machine.create_ssh_service('ssh service')
services = machine.list_services()

# Create SSH Keypair
keypair = org.create_ssh_key_pair('fromcli', 'cloud-user', '/home/vagrant/key.pem')

# Associate SSH Keypair with Machine
machine.associate_key_pair(keypair)

# Enable test with no configuration
machine = org.create_machine(ip='192.168.53.9')
machine.enable_test('sslyze-heartbleed')
machine.scan()

# Enable test with extra configuration
org.enable_test(
    'qualys',
    {'username': 'qualys-user', 'password': 'secret123', 'option_title': 'Norad Unauthenticated', 'scanner': 70270, 'report_template_id': 1880200}
)
org.scan()

# Update machine properties
machine.update(description='Tested via Python')
results = machine.black_box(latest=True)
print(results)
```
