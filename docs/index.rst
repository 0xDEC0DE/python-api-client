.. norad documentation master file, created by
   sphinx-quickstart on Wed Jun 29 13:19:27 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to norad's documentation!
=================================

.. toctree::
   :maxdepth: 2

Introduction
------------
Welcome the API docs for Norad's Python SDK!

For more details, please see https://norad-gitlab.cisco.com/norad/python-sdk.

Basic workflow
--------------
- Initialize a `norad.api.Client` with your API token
- Use methods on the client to get `Organization` and `Machine` objects
- Use the methods on `Organization` and `Machine` objects

norad.api
---------
.. automodule:: norad.api
   :members:

norad.models
------------
.. automodule:: norad.models
   :members:


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

